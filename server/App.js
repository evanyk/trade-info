const express = require('express');
const http = require('http');
const WebSocket = require('ws');
const cors = require('cors');
const nj = require('numjs');
const url = require('url');

const app = express();

//Access-Control
app.use( cors() );
//http://localhost:9001/AAPL.csv
app.use( express.static('mock') );

const server = http.createServer(app);

const ws1= new WebSocket.Server({ noServer: true });

function smallThan(x, y){
    if(x[1] < y[1]){
        return -1;
    } else if(x[1] > y[1]){
        return 1;
    }else{
        return 0;
    }
}

function largeThan(x, y){
    if(x[1] > y[1]){
        return -1;
    } else if(x[1] < y[1]){
        return 1;
    }else{
        return 0;
    }
}

const ask = nj.random([10,2]).tolist();
ask.sort(largeThan);
const bid = nj.random([10,2]).tolist();
bid.sort(smallThan);

function update( arr , method ){
    var rand = (nj.random().get(0) * 10 ).toFixed(0) ;
    var nArr = nj.random([1,2]).tolist();
    arr[rand] = nArr[0];    
    arr.sort(method);
}

//websocket test
ws1.on('connection', (ws) => {
    ws.on('message', (msg) => {
        ws.send(`Re: ${ new Date() } -> ${ msg }`);
        
    });
    ws.on('close', () => {
        console.log('close');
    });

    ws.broadcast = function broadcast(data) {
        wsserver.clients.forEach(function each(client) {
          if (client.readyState === WebSocket.OPEN) {
            client.send(data);
          }
        });
      };
    //when the socket is connected, send message per second
    setInterval(() => {
        // ws.send(`Re: ${ new Date() }`);
        //ws.broadcast(`Re: ${ new Date() }`);
        var rand = nj.random().get(0);
        if( rand < 0.2 ){
            update(ask, largeThan);
            update(bid, smallThan);
            if(ws.readyState === 1 ){
                ws.send(JSON.stringify({
                    ask: ask,
                    bid: bid
                }));
            }
        }
        
    },200);
    
});

server.on('upgrade', function upgrade(req, sock, head){
    const pathname = url.parse(req.url).pathname;
    if( pathname === '/ws/board' ){
        ws1.handleUpgrade(req, sock, head, function done(ws){
            ws1.emit('connection',ws,req);
        });
    }
});

server.listen(9001, () => {
    console.log('server started on 9001');
});