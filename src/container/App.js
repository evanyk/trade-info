import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { reduce } from '../reducer/Action.reducer';
import Header from './Header.container';
import CandleChart from '../component/CandleChart.component';
import Board from '../component/Board.component';


const store = createStore(reduce);

class App extends Component {  
  render() {
    const styles = {
      column: {
        display: 'flex',
        flexDirection: 'column',
        backgroundColor: '#FFFFFF'
      },
      row: {
				flex: 1,
				display: 'flex',
				flexDirection: 'row'
			}
    }

    return (
        <Provider store={ store }>
          <div style={styles.column}>
            <div style={styles.row}>
              <Header />  
            </div>
            <div style={styles.row}>                        
              {/* <Router>
                <Switch>
                  <Route path="/" component={ CandleChart }></Route>
                </Switch>
              </Router> */}
              <CandleChart />
              <Board />
            </div>
          </div>
        </Provider>   
    );
  }
}

export default App;
