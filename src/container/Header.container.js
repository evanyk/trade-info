import React, { Component } from 'react';
import {
    Navbar,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
} from 'reactstrap';

const axios = require('axios');



export default class Header extends Component {
    constructor(props){
        super(props);

        this.toggle = this.toggle.bind(this);
        this.styles = {
            bkColor: {
                backgroundColor: 'rgb(36,40,42)'
            },
            fontColor: {
                color: 'white'
            },
            flex: {
                flex: 1
            }
        }
    }

    componentDidMount(){
        
    }

    toggle(){
        this.setState({
            isExtended: !this.state.collapsed
        });
    }

    render() {
        return (
            <div style={this.styles.flex}>
                <Navbar>
                    <NavbarBrand href="#">EX Coin</NavbarBrand>
                    <Nav>
                        <NavItem>
                            <NavLink href="#">Home</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink href="#">About</NavLink>
                        </NavItem>
                    </Nav>
                    <Nav right>
                    <NavItem>
                            <NavLink href="#">Sign In</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink href="#">Sign Up</NavLink>
                        </NavItem>
                    </Nav>
                </Navbar>
            </div>
        );
    }
}