import React from "react";
import PropTypes from "prop-types";

import { format } from "d3-format";
import { timeFormat } from "d3-time-format";

import { ChartCanvas, Chart } from "react-stockcharts";
import {
	BarSeries,
	CandlestickSeries,
	LineSeries,
	RSISeries,
} from "react-stockcharts/lib/series";
import { XAxis, YAxis } from "react-stockcharts/lib/axes";
import { EdgeIndicator, 
	CrossHairCursor,
	CurrentCoordinate,
	MouseCoordinateX,
	MouseCoordinateY, } from "react-stockcharts/lib/coordinates";

import { discontinuousTimeScaleProvider } from "react-stockcharts/lib/scale";
import { HoverTooltip } from "react-stockcharts/lib/tooltip";
import { ema, wma, sma, tma, rsi, atr  } from "react-stockcharts/lib/indicator";
import { fitWidth } from "react-stockcharts/lib/helper";
import { last } from "react-stockcharts/lib/utils";
import {
	OHLCTooltip,
	MovingAverageTooltip,
	RSITooltip,
	SingleValueTooltip,
} from "react-stockcharts/lib/tooltip";

const dateFormat = timeFormat("%Y-%m-%d");
const numberFormat = format(".2f");

function tooltipContent(ys) {
	return ({ currentItem, xAccessor }) => {
		return {
			x: dateFormat(xAccessor(currentItem)),
			y: [
				{
					label: "open",
					value: currentItem.open && numberFormat(currentItem.open)
				},
				{
					label: "high",
					value: currentItem.high && numberFormat(currentItem.high)
				},
				{
					label: "low",
					value: currentItem.low && numberFormat(currentItem.low)
				},
				{
					label: "close",
					value: currentItem.close && numberFormat(currentItem.close)
				}
			]
				.concat(
					ys.map(each => ({
						label: each.label,
						value: each.value(currentItem),
						stroke: each.stroke
					}))
				)
				.filter(line => line.value)
		};
	};
}

const keyValues = ["high", "low"];

class ChartCom extends React.Component {
	removeRandomValues(data) {
		return data.map(item => {
			const newItem = { ...item };
			const numberOfDeletion =
				Math.floor(Math.random() * keyValues.length) + 1;
			for (let i = 0; i < numberOfDeletion; i += 1) {
				const randomKey =
					keyValues[Math.floor(Math.random() * keyValues.length)];
				newItem[randomKey] = undefined;
			}
			return newItem;
		});
	}

	render() {
		let { type, data: initialData, width, ratio } = this.props;

		// remove some of the data to be able to see
		// the tooltip resize
		initialData = this.removeRandomValues(initialData);

		const ema26 = ema()
			.id(0)
			.options({ windowSize: 26 })
			.merge((d, c) => {
				d.ema26 = c;
			})
			.accessor(d => d.ema26);

		const ema12 = ema()
			.id(1)
			.options({ windowSize: 12 })
			.merge((d, c) => {
				d.ema12 = c;
			})
			.accessor(d => d.ema12);

		const rsiCalculator = rsi()
			.options({ windowSize: 14 })
			.merge((d, c) => {d.rsi = c;})
			.accessor(d => d.rsi);

		const margin = { left: 80, right: 80, top: 30, bottom: 50 };

		const calculatedData = ema26(ema12(rsiCalculator(initialData)));
		const xScaleProvider = discontinuousTimeScaleProvider.inputDateAccessor(
			d => d.date
		);
		const { data, xScale, xAccessor, displayXAccessor } = xScaleProvider(
			calculatedData
		);

		const start = xAccessor(last(data));
		const end = xAccessor(data[Math.max(0, data.length - 150)]);
		const xExtents = [start, end];

		return (
			<ChartCanvas
				height={700}
				width={width}
				ratio={ratio}
				margin={margin}
				type={type}
				seriesName="MSFT"
				data={data}
				xScale={xScale}
				xAccessor={xAccessor}
				displayXAccessor={displayXAccessor}
				xExtents={xExtents}
			>
				<Chart
					id={1}
					height={450}
					yExtents={[
						d => [d.high, d.low],
						ema26.accessor(),
						ema12.accessor()
					]}
					padding={{ top: 2, bottom: 5 }}
				>
					<XAxis axisAt="bottom" orient="bottom" />

					<YAxis axisAt="right" orient="right" ticks={5} />
					<MouseCoordinateX
						at="bottom"
						orient="bottom"
						displayFormat={timeFormat("%Y-%m-%d")} />
					<MouseCoordinateY
						at="right"
						orient="right"
						displayFormat={format(".2f")} />
					{/* <CrossHairCursor /> */}
					<CandlestickSeries />
					<LineSeries
						yAccessor={ema26.accessor()}
						stroke={ema26.stroke()}
					/>
					<LineSeries
						yAccessor={ema12.accessor()}
						stroke={ema12.stroke()}
					/>

					<CurrentCoordinate yAccessor={ema26.accessor()} fill={ema26.stroke()} />
					<CurrentCoordinate yAccessor={ema12.accessor()} fill={ema12.stroke()} />

					<EdgeIndicator itemType="last" orient="right" edgeAt="right"
						yAccessor={ema26.accessor()} fill="#ff7f0e"
						 />

					<EdgeIndicator itemType="last" orient="right" edgeAt="right"
						yAccessor={ema12.accessor()} fill="#2ca02c"
						displayFormat={format(".0%")} />

					<EdgeIndicator
						itemType="last"
						orient="right"
						edgeAt="right"
						yAccessor={d => d.close}
						fill={d => (d.close > d.open ? "#6BA583" : "#FF0000")}
					/>

					<HoverTooltip
						yAccessor={ema12.accessor()}
						tooltipContent={tooltipContent([
							{
								label: `${ema26.type()}(${ema26.options()
									.windowSize})`,
								value: d => numberFormat(ema26.accessor()(d)),
								stroke: ema26.stroke()
							},
							{
								label: `${ema12.type()}(${ema12.options()
									.windowSize})`,
								value: d => numberFormat(ema12.accessor()(d)),
								stroke: ema12.stroke()
							}
						])}
						fontSize={15}
					/>
					<OHLCTooltip origin={[0, 0]}/>
					<MovingAverageTooltip
						onClick={e => console.log(e)}
						origin={[0, 15]}
						options={[
							{
								yAccessor: ema26.accessor(),
								type: "EMA",
								stroke: ema26.stroke(),
								windowSize: ema26.options().windowSize,
								echo: "some echo here",
							},
							{
								yAccessor: ema12.accessor(),
								type: "EMA",
								stroke: ema12.stroke(),
								windowSize: ema12.options().windowSize,
								echo: "some echo here",
							},
						]}
					/>
				</Chart>
				<Chart id={3}
					yExtents={[0, 100]}
					height={150} origin={(w, h) => [0, h - 150]}
				>
					<XAxis axisAt="bottom" orient="bottom" showTicks={false} outerTickSize={0} />
					<YAxis axisAt="right"
						orient="right"
						tickValues={[30, 50, 70]}/>
					<MouseCoordinateY
						at="right"
						orient="right"
						displayFormat={format(".2f")} />

					<RSISeries yAccessor={d => d.rsi} />

					<RSITooltip origin={[0, 15]}
						yAccessor={d => d.rsi}
						options={rsiCalculator.options()} />
				</Chart>				
				<CrossHairCursor />
			</ChartCanvas>
		);
	}
}

ChartCom.propTypes = {
	data: PropTypes.array.isRequired,
	width: PropTypes.number.isRequired,
	ratio: PropTypes.number.isRequired,
	type: PropTypes.oneOf(["svg", "hybrid"]).isRequired
};

ChartCom.defaultProps = {
	type: "svg"
};
ChartCom = fitWidth(ChartCom);

export default ChartCom;
