import React, {Component} from 'react';

class Cell extends Component {
    constructor(props){
        super(props);
        this.state = {};
        // console.log(this.state);
    }

    componentDidMount(){
        
        console.log('mount -> ',this.state);
    }

    componentWillReceiveProps(){
        this.setState(this.props);
        if(this.state.className){
            this.setState({
                value: this.state.value,
                basicClass: this.state.basicClass,
                className: null
            })
        }
    }

    render(){
        return (
            <div  className={this.state.basicClass + ' ' +this.state.className}>{this.state.value}</div>
        )
    }
}

export default Cell;