import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { getData } from '../utls/WGet.util';
import ChartCom from './Chart.component';

import { TypeChooser } from "react-stockcharts/lib/helper";


export default class CandleChart extends Component {
    componentDidMount(){
		this.styles = {
			flex: 3,
			minWidth: '1000px',
			backgroundColor: '#FEFEFE',
			border: '1px solid rgb(24,32,34)'
		}

        getData().then( data => {
			console.log(data);
            this.setState({data})
        })
	}
	
    render () {
        if (this.state == null) {
			return <div>Loading...</div>
		}
		return (
			<div style={ this.styles }>
				<ChartCom type={'hybrid'} data={this.state.data} />
			</div>			
		)
    }
} 