import React, {Component} from 'react';
import '../static/css/Board.css';
import Cell from './Cell.component';

class Board extends Component {
    constructor(props){
        super(props);
        this.state = {
            data: {
                ask: [],
                bid: []
            }
        }

        this.ws = new WebSocket("ws://localhost:9001/ws/board");
        
        this.ws.onmessage = (evt) => {
            var data = JSON.parse(evt.data);
            this.setState({
                data: data
            });
        }
    }

    render(){
        const askList =  this.state.data.ask.map( (item,index) => {
            return (
                <tr>
                    <td key={'ask_sell_' + index}>
                        <Cell  className="quantity-cell" value={ Array.isArray(item) ? Number(item[0]).toFixed(8) : '' } />
                    </td>
                    
                    <td key={'ask_price_' + index}>
                        <Cell basicClass="red-font" className="price-cell" value={ Array.isArray(item) ? Number(item[1]).toFixed(8) : '' } />
                    </td>
                    <td key={'ask_ot_' + index}>
                        <Cell  className="quantity-cell" value='' />
                    </td>
                </tr>
            )
        }) ;
        const bidList = this.state.data.bid.map( (item,index) => {
            return (
                <tr>
                    <td key={'bid_ot_' + index} className="quantity-cell">
                        <Cell  className="quantity-cell" value='' />
                    </td>
                    <td key={'bid_price_' + index}>
                        <Cell basicClass="red-font" className="price-cell" value={ Array.isArray(item) ? Number(item[1]).toFixed(8) : '' } />
                    </td>
                    <td key={'bid_buy_' + index}>
                        <Cell  className="quantity-cell" value={ Array.isArray(item) ? Number(item[0]).toFixed(8) : '' } />
                    </td>
                </tr>
            )
        }); 
        return (
            <div className="board-box">
                <table>
                    <thead>
                        <tr>
                            {/* quantity */}
                            <th>Sell Qty</th>
                            <th>Price</th>
                            <th>Buy Qty</th>
                        </tr>
                    </thead>
                    <tbody>
                        { askList }
                        { bidList }
                    </tbody>
                </table>
            </div>
        );
    }
}

export default Board;