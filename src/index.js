import React from 'react';
import ReactDOM from 'react-dom';
import './static/css/index.css';
import './static/css/bootstrap.min.css';
import './static/css/bootstrap-grid.min.css';
import App from './container/App';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
