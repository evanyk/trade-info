import { RENDER } from '../constant/Action.constant';

const initState = {
    data: []
}

export const reduce = (state = initState, action) => {
    switch (action.type){
        case RENDER:
            return Object.assign({}, state, {
                data: [
                    ...state.data
                ],
                updated: true
            });
        default:
            return state;
    }
}