import { RENDER } from '../constant/Action.constant';

export const render = () => {
    return {
        type: RENDER
    }
}
